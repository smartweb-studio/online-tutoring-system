#!/bin/bash
set -e

cd /home/ots/www

BUILD_NUMBER=${BUILD_NUMBER}
DB_NAME="ots_$BUILD_NUMBER"
readonly DB_NAME

SNIFFERS_PATH="sniffers/$BUILD_NUMBER/"
mkdir $SNIFFERS_PATH

BUILD_NUMBER="builds/$BUILD_NUMBER"
readonly BUILD_NUMBER

# Fetch all latest data from the repo.
git pull origin ${sourceBranch} && git checkout ${sourceBranch}

#phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme /file/to/test/index.php
#echo "CodeSniffer: Drupal standard file ./Drupalsniff.txt" >> {{ workspace_root }}/{{ artifacts_file }} && phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme -n profiles/ots --report-file=Drupalsniff.txt --ignore={{ ignore_path }}

# Drupal coding standarts
echo "CodeSniffer: profile standard file ./$SNIFFERS_PATH/profilesniff.txt" >> ./$SNIFFERS_PATH/profilesniff.txt && phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme -n --report-file=./$SNIFFERS_PATH/profilesniff.txt --ignore=src/profiles/ots/themes/custom/goodnex src/profiles/ots
if [ $? != 0 ]
	then
		echo "Fix Drupal coding standards."
		exit 1
	fi

# DrupalPractice
echo "DrupalPractice CodeSniffer: profile standard file ./$SNIFFERS_PATH/DrupalPractice_profile_sniff.txt" >> ./$SNIFFERS_PATH/DrupalPractice && phpcs --standard=DrupalPractice --extensions=php,module,inc,install,test,profile,theme -n --report-file=./$SNIFFERS_PATH/DrupalPractice_profile_sniff.txt --ignore=src/profiles/ots/themes/custom/goodnex src/profiles/ots
if [ $? != 0 ]
	then
		echo "Fix DrupalPractice coding standards."
		exit 1
	fi

#JSHint
#echo "JSHint: profile standard file ./profilejshint.txt" >> ./profilejshint.txt && find src/profiles/ots/modules ! -path "*mute*" -type f \( -iname "*.js" ! -iname "*min.js" \) -print0 | xargs -0 jshint > profilejshint.txt

#Create build
drush make --contrib-destination=profiles/ots ./src/profiles/ots/ots.make $BUILD_NUMBER
cp -r ./src/profiles/ots ./$BUILD_NUMBER/profiles

echo "create database $DB_NAME" | mysql -umaxim -pq2w1e4r3

cd ./$BUILD_NUMBER
pwd
drush si ots --db-url=mysql://maxim:q2w1e4r3@localhost/$DB_NAME --account-name=admin --account-pass=admin -y
drush features-revert-all -y
drush env-switch development
