(function ($) {

  jQuery(document).ready(function($) {
    if($('.lesson-plan[active="true"]').length >= 3) {
      $('#ots-common-add-lesson-plan-form').hide();
    }
    else {
      $('#ots-common-add-lesson-plan-form').show();
    }

    $('#ots-common-form #edit-submit').click(function(event) {
      event.preventDefault();
      var year = $('#lesson-planner-year-filter').val();
      var month = $('#lesson-planner-month-filter').val();
      var week = $('#lesson-planner-week-filter').val();

      if(year != '') {
        $('.lesson-plan').each(function() {
          if($(this).attr('id').match(year) == null) {
            $(this).attr('active', 'false').hide();
          }
          else {
            $(this).attr('active', 'true').show()
          }
        });
      }
      else {
        $('.lesson-plan').each(function() {
          $(this).attr('active', 'true').show();
        });
      }

      if(month != '') {
        $('.lesson-plan').each(function() {
          if($(this).attr('id').match(month) == null) {
            $(this).attr('active', 'false').hide();
          }
        });
      }

      if(week != '') {
        $('.lesson-plan').each(function() {
          if($(this).attr('id').match(week) == null) {
            $(this).attr('active', 'false').hide();
          }
        });
      }

      $current_week = null;

      $('#lesson-planner-week-filter').children().each(function() {
        if($(this).text().match('c') != null) {
          $current_week = $(this).val();
        }
      });

      if($('#lesson-planner-week-filter').val() == $current_week) {
        if($('.lesson-plan[active="true"]').length >= 3) {
          $('#ots-common-add-lesson-plan-form').hide();
        }
        else {
          $('#ots-common-add-lesson-plan-form').show();
        }
      }
      else {
        $('#ots-common-add-lesson-plan-form').hide();
      }

    });
  });

})(jQuery);
