<?php

/**
 * @file
 * OTS Common Pages.
 */

/**
 * Callback function for lesson planner page.
 */
function lesson_planner_callback($student) {
  global $user;
  $rows = array();
  $render_array = array();
  $week_filter_items = array('' => t('None'));

  $render_array[] = drupal_get_form('ots_common_form', $student);

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'lesson_plan')
    ->fieldCondition('field_lesson_plan_student', 'target_id', $student->uid)
    ->propertyCondition('status', NODE_PUBLISHED)
    ->propertyCondition('uid', $user->uid);

  $result = $query->execute();

  if (isset($result['node'])) {
    $items_nids = array_keys($result['node']);
    $items = entity_load('node', $items_nids);

    foreach($items as $key => $value) {
      $date = substr($value->field_date_from[LANGUAGE_NONE][0]['value'], 0, 10);

      $rows[$key][$date][] = array(
        'Title',
        $value->title
        );
      $rows[$key][$date][] = array(
        'Objectives',
        $value->field_objectives[LANGUAGE_NONE][0]['value']
        );
      $rows[$key][$date][] = array(
        'Key tasks',
        $value->field_key_tasks[LANGUAGE_NONE][0]['value']
        );
      $rows[$key][$date][] = array(
        'Resources',
        $value->field_resources[LANGUAGE_NONE][0]['value']
        );
      $rows[$key][$date][] = array(
        'Outcome',
        $value->field_outcome[LANGUAGE_NONE][0]['value']
        );
      $rows[$key][$date][] = array(
        'Notes',
        $value->field_notes[LANGUAGE_NONE][0]['value']
        );
    }

    foreach($rows as $r) {
      foreach($r as $key => $value) {
        $render_array[] = array(
          '#theme' => 'table',
          '#rows' => $value,
          '#attributes' => array('id' => $key, 'class' => 'lesson-plan', 'active' => 'true')
        );
      }
    }
  }

  $render_array[] = drupal_get_form('ots_common_add_lesson_plan_form', $student);

  return $render_array;
}
