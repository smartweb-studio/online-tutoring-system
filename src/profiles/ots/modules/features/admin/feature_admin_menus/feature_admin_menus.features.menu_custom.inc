<?php
// @codingStandardsIgnoreFile
/**
 * @file
 * feature_admin_menus.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function feature_admin_menus_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-admin-menu.
  $menus['menu-admin-menu'] = array(
    'menu_name' => 'menu-admin-menu',
    'title' => 'Administrator dashboard menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Administrator dashboard menu');


  return $menus;
}
