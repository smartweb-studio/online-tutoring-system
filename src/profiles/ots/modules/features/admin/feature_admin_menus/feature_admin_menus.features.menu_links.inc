<?php
// @codingStandardsIgnoreFile
/**
 * @file
 * feature_admin_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function feature_admin_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-admin-menu_accounts:http://dev-ots.rocksolid.com.ua/user/admin/accounts
  $menu_links['menu-admin-menu_accounts:http://dev-ots.rocksolid.com.ua/user/admin/accounts'] = array(
    'menu_name' => 'menu-admin-menu',
    'link_path' => 'http://dev-ots.rocksolid.com.ua/user/admin/accounts',
    'router_path' => '',
    'link_title' => 'Accounts',
    'options' => array(
      'attributes' => array(
        'title' => '',
        'class' => array(
          0 => 'menu_icon',
          1 => 'menu-519',
        ),
      ),
      'menu_icon' => array(
        'enable' => 1,
        'path' => 'public://menu_icons/menu_icon_519.png',
        'image_style' => 'menu_icon',
      ),
      'menu_token_link_path' => 'user/[current-user:name]/accounts',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'alter' => TRUE,
      'identifier' => 'menu-admin-menu_accounts:http://dev-ots.rocksolid.com.ua/user/admin/accounts',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 1,
  );
  // Exported menu link: menu-admin-menu_add-user:<front>
  $menu_links['menu-admin-menu_add-user:<front>'] = array(
    'menu_name' => 'menu-admin-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Add User',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'sites/all/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-admin-menu_add-user:<front>',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 1,
  );
  // Exported menu link: menu-admin-menu_discussion-board:<front>
  $menu_links['menu-admin-menu_discussion-board:<front>'] = array(
    'menu_name' => 'menu-admin-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Discussion Board',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'sites/all/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-admin-menu_discussion-board:<front>',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 5,
    'customized' => 1,
  );
  // Exported menu link: menu-admin-menu_leave-calendar:<front>
  $menu_links['menu-admin-menu_leave-calendar:<front>'] = array(
    'menu_name' => 'menu-admin-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Leave Calendar',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'sites/all/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-admin-menu_leave-calendar:<front>',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 4,
    'customized' => 1,
  );
  // Exported menu link: menu-admin-menu_students:http://dev-ots.rocksolid.com.ua/user/admin/students
  $menu_links['menu-admin-menu_students:http://dev-ots.rocksolid.com.ua/user/admin/students'] = array(
    'menu_name' => 'menu-admin-menu',
    'link_path' => 'http://dev-ots.rocksolid.com.ua/user/admin/students',
    'router_path' => '',
    'link_title' => 'Students',
    'options' => array(
      'attributes' => array(
        'title' => '',
        'class' => array(
          0 => 'menu_icon',
          1 => 'menu-574',
        ),
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 1,
        'path' => 'public://menu_icons/menu_icon_574.png',
        'image_style' => 'menu_icon',
      ),
      'menu_token_link_path' => 'user/[current-user:name]/students',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'identifier' => 'menu-admin-menu_students:http://dev-ots.rocksolid.com.ua/user/admin/students',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-admin-menu_tutors:http://dev-ots.rocksolid.com.ua/user/admin/tutors
  $menu_links['menu-admin-menu_tutors:http://dev-ots.rocksolid.com.ua/user/admin/tutors'] = array(
    'menu_name' => 'menu-admin-menu',
    'link_path' => 'http://dev-ots.rocksolid.com.ua/user/admin/tutors',
    'router_path' => '',
    'link_title' => 'Tutors',
    'options' => array(
      'menu_token_link_path' => 'user/[current-user:name]/tutors',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
        'class' => array(
          0 => 'menu_icon',
          1 => 'menu-577',
        ),
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 1,
        'path' => 'public://menu_icons/menu_icon_577.jpg',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-admin-menu_tutors:http://dev-ots.rocksolid.com.ua/user/admin/tutors',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Accounts');
  t('Add User');
  t('Discussion Board');
  t('Leave Calendar');
  t('Students');
  t('Tutors');

  return $menu_links;
}
