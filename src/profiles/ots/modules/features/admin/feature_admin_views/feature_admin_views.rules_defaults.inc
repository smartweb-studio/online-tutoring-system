<?php
/**
 * @file
 * feature_admin_views.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function feature_admin_views_default_rules_configuration() {
  $items = array();
  $items['rules_unblock_selected_users'] = entity_import('rules_config', '{ "rules_unblock_selected_users" : {
      "LABEL" : "Unblock selected users",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "vbo_unblock_users" : { "label" : "Unblock selected users", "type" : "user" } },
      "ACTION SET" : [ { "user_unblock" : { "account" : [ "vbo-unblock-users" ] } } ]
    }
  }');
  return $items;
}
