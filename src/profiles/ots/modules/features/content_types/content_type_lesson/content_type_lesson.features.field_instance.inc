<?php
/**
 * @file
 * content_type_lesson.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function content_type_lesson_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-lesson-field_actual_grammar'
  $field_instances['node-lesson-field_actual_grammar'] = array(
    'bundle' => 'lesson',
    'default_value' => array(
      0 => array(
        'value' => -1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'editablefields',
        'settings' => array(
          'click_to_edit' => 0,
          'empty_text' => 'text',
          'fallback_format' => 'number_integer',
          'fallback_settings' => array(
            'prefix_suffix' => 1,
            'thousand_separator' => ' ',
          ),
        ),
        'type' => 'editable',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_actual_grammar',
    'label' => 'Actual Grammar',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => -1,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-lesson-field_actual_vocab'
  $field_instances['node-lesson-field_actual_vocab'] = array(
    'bundle' => 'lesson',
    'default_value' => array(
      0 => array(
        'value' => -1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'editablefields',
        'settings' => array(
          'click_to_edit' => FALSE,
          'empty_text' => '',
          'fallback_format' => NULL,
          'fallback_settings' => array(),
        ),
        'type' => 'editable',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_actual_vocab',
    'label' => 'Actual Vocab',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => -1,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-lesson-field_completion'
  $field_instances['node-lesson-field_completion'] = array(
    'bundle' => 'lesson',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'workflowfield',
        'settings' => array(
          'comment' => 1,
          'name_as_title' => 1,
        ),
        'type' => 'workflow_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_completion',
    'label' => 'Completion',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => 1,
      ),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-lesson-field_date'
  $field_instances['node-lesson-field_date'] = array(
    'bundle' => 'lesson',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_date',
    'label' => 'Date',
    'required' => 1,
    'settings' => array(
      'date_popup_timepicker' => array(
        'timepicker' => array(
          'timepicker_options' => array(
            'amPmText' => array(
              0 => 'AM',
              1 => 'PM',
            ),
            'closeButtonText' => 'Done',
            'defaultTime' => 'now',
            'deselectButtonText' => 'Deselect',
            'hourText' => 'Hour',
            'hours' => array(
              'ends' => 23,
              'starts' => 0,
            ),
            'minuteText' => 'Minute',
            'minutes' => array(
              'ends' => 55,
              'interval' => 5,
              'starts' => 0,
            ),
            'nowButtonText' => 'Now',
            'periodSeparator' => ' ',
            'rows' => 4,
            'showCloseButton' => FALSE,
            'showDeselectButton' => FALSE,
            'showHours' => TRUE,
            'showLeadingZero' => TRUE,
            'showMinutes' => TRUE,
            'showMinutesLeadingZero' => TRUE,
            'showNowButton' => FALSE,
            'showOn' => 'focus',
            'showPeriod' => FALSE,
            'showPeriodLabels' => TRUE,
            'timeSeparator' => ':',
          ),
        ),
      ),
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'restrictions' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(
            'FR' => 0,
            'MO' => 0,
            'SA' => 0,
            'SU' => 0,
            'TH' => 0,
            'TU' => 0,
            'WE' => 0,
          ),
        ),
      ),
      'restrictions2' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(),
          'weekdays_host_entity' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
          'weekdays_product_display' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'Y-m-d H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'repeat_collapsed' => 1,
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-lesson-field_predicted_grammar'
  $field_instances['node-lesson-field_predicted_grammar'] = array(
    'bundle' => 'lesson',
    'default_value' => array(
      0 => array(
        'value' => -1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'editablefields',
        'settings' => array(
          'click_to_edit' => FALSE,
          'empty_text' => '',
          'fallback_format' => NULL,
          'fallback_settings' => array(),
        ),
        'type' => 'editable',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_predicted_grammar',
    'label' => 'Predicted Grammar',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => -1,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-lesson-field_predicted_vocab'
  $field_instances['node-lesson-field_predicted_vocab'] = array(
    'bundle' => 'lesson',
    'default_value' => array(
      0 => array(
        'value' => -1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'editablefields',
        'settings' => array(
          'click_to_edit' => FALSE,
          'empty_text' => '',
          'fallback_format' => NULL,
          'fallback_settings' => array(),
        ),
        'type' => 'editable',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_predicted_vocab',
    'label' => 'Predicted Vocab',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => -1,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-lesson-field_state'
  $field_instances['node-lesson-field_state'] = array(
    'bundle' => 'lesson',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'workflowfield',
        'settings' => array(
          'comment' => 1,
          'name_as_title' => 1,
        ),
        'type' => 'workflow_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_state',
    'label' => 'State',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => 1,
      ),
      'type' => 'options_select',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-lesson-field_student'
  $field_instances['node-lesson-field_student'] = array(
    'bundle' => 'lesson',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_student',
    'label' => 'Student',
    'required' => 1,
    'settings' => array(
      'nodeaccess_userreference' => array(
        'all' => array(
          'view' => 0,
        ),
        'all_published' => 0,
        'author' => array(
          'delete' => 0,
          'update' => 0,
          'view' => 'view',
        ),
        'author_published' => 0,
        'create' => array(
          'article' => 0,
          'lesson' => 0,
          'page' => 0,
        ),
        'priority' => 0,
        'referenced' => array(
          'delete' => 0,
          'deny_delete' => 0,
          'deny_update' => 0,
          'deny_view' => 0,
          'update' => 0,
          'view' => 'view',
        ),
        'referenced_published' => 0,
        'unused' => 0,
        'views' => array(
          'view' => '',
          'view_args' => '',
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => 1,
      ),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Actual Grammar');
  t('Actual Vocab');
  t('Completion');
  t('Date');
  t('Predicted Grammar');
  t('Predicted Vocab');
  t('State');
  t('Student');

  return $field_instances;
}
