<?php
/**
 * @file
 * content_type_lesson.features.inc
 */

/**
 * Implements hook_default_Workflow().
 */
function content_type_lesson_default_Workflow() {
  $workflows = array();

  // Exported workflow: 'accepted_declined'
  $workflows['accepted_declined'] = entity_import('Workflow', '{
    "name" : "accepted_declined",
    "tab_roles" : [],
    "options" : [],
    "states" : {
      "8" : {"sid":"8","wid":"3","weight":"-50","sysid":"1","state":"(creation)","status":"1","name":"(creation)"},
      "9" : {"sid":"9","wid":"3","weight":"-19","sysid":"0","state":"None","status":"1","name":"none"},
      "10" : {"sid":"10","wid":"3","weight":"-18","sysid":"0","state":"Accepted","status":"1","name":"accepted"},
      "11" : {"sid":"11","wid":"3","weight":"-17","sysid":"0","state":"Declined","status":"1","name":"declined"}
    },
    "transitions" : {
      "11" : {"tid":"11","sid":"8","target_sid":"9","roles":{"-1":-1},"wid":"3","name":"1_4","label":"None"},
      "12" : {"tid":"12","sid":"9","target_sid":"9","roles":{"-1":-1,"4":"4","5":"5","6":"6"},"wid":"3","name":"4_4","label":"None"},
      "13" : {"tid":"13","sid":"9","target_sid":"10","roles":{"4":"4"},"wid":"3","name":"4_2","label":"Accepted"},
      "14" : {"tid":"14","sid":"9","target_sid":"11","roles":{"4":"4"},"wid":"3","name":"4_3","label":"Declined"},
      "15" : {"tid":"15","sid":"10","target_sid":"10","roles":{"-1":-1,"4":"4","5":"5","6":"6"},"wid":"3","name":"2_2","label":"Accepted"},
      "16" : {"tid":"16","sid":"11","target_sid":"11","roles":{"-1":-1,"4":"4","5":"5","6":"6"},"wid":"3","name":"3_3","label":"Declined"}
    },
    "label" : "Accepted-Declined",
    "typeMap" : [],
    "wid_original" : "3",
    "system_roles" : {
      "-1" : "(author)",
      "1" : "anonymous user",
      "2" : "authenticated user",
      "4" : "student",
      "5" : "tutor",
      "6" : "administrator",
      "3" : "curator"
    }
  }');

  // Exported workflow: 'completed_incompleted'
  $workflows['completed_incompleted'] = entity_import('Workflow', '{
    "name" : "completed_incompleted",
    "tab_roles" : [],
    "options" : [],
    "states" : {
      "12" : {"sid":"12","wid":"4","weight":"-50","sysid":"1","state":"(creation)","status":"1","name":"(creation)"},
      "13" : {"sid":"13","wid":"4","weight":"-20","sysid":"0","state":"Incompleted","status":"1","name":"incompleted"},
      "14" : {"sid":"14","wid":"4","weight":"-19","sysid":"0","state":"Completed","status":"1","name":"completed"}
    },
    "transitions" : {
      "17" : {"tid":"17","sid":"12","target_sid":"13","roles":{"-1":-1,"5":"5"},"wid":"4","name":"9_10","label":"Incompleted"},
      "18" : {"tid":"18","sid":"13","target_sid":"13","roles":{"-1":-1,"4":"4","5":"5","6":"6"},"wid":"4","name":"10_10","label":"Incompleted"},
      "19" : {"tid":"19","sid":"13","target_sid":"14","roles":{"5":"5"},"wid":"4","name":"10_11","label":"Completed"},
      "20" : {"tid":"20","sid":"14","target_sid":"14","roles":{"-1":-1,"4":"4","5":"5","6":"6"},"wid":"4","name":"11_11","label":"Completed"}
    },
    "label" : "Completed-Incompleted",
    "typeMap" : [],
    "wid_original" : "4",
    "system_roles" : {
      "-1" : "(author)",
      "1" : "anonymous user",
      "2" : "authenticated user",
      "4" : "student",
      "5" : "tutor",
      "6" : "administrator",
      "3" : "curator"
    }
  }');

  return $workflows;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function content_type_lesson_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function content_type_lesson_node_info() {
  $items = array(
    'lesson' => array(
      'name' => t('Lesson'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
