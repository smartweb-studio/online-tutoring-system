<?php
/**
 * @file
 * content_type_lesson.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function content_type_lesson_default_rules_configuration() {
  $items = array();
  $items['rules_lesson_creating_notification'] = entity_import('rules_config', '{ "rules_lesson_creating_notification" : {
      "LABEL" : "Lesson Creating Notification",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--lesson" : { "bundle" : "lesson" } },
      "DO" : [
        { "mail" : {
            "to" : [ "node:field-student:mail" ],
            "subject" : "Online Tutoring System Notification",
            "message" : "New lesson has been created and assigned on you! Please review lesson\\u0027s details and accept\\/decline it.\\u003C\\/br\\u003E\\r\\n[node:url]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_lesson_reminder'] = entity_import('rules_config', '{ "rules_lesson_reminder" : {
      "LABEL" : "Lesson Reminder",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "node" : { "label" : "node", "type" : "node" },
        "user" : { "label" : "user", "type" : "user" }
      },
      "ACTION SET" : [
        { "mail" : {
            "to" : [ "user:mail" ],
            "subject" : "Lesson Reminder",
            "message" : "Don\\u0027t forget about lesson on [node:field-date]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_reminder_before_lesson'] = entity_import('rules_config', '{ "rules_reminder_before_lesson" : {
      "LABEL" : "Reminder Before Lesson",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "rules_scheduler" ],
      "ON" : { "node_insert--lesson" : { "bundle" : "lesson" } },
      "DO" : [
        { "LOOP" : {
            "USING" : { "list" : [ "node:field-date" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : [
              { "schedule" : {
                  "component" : "rules_lesson_reminder",
                  "date" : { "select" : "list-item", "date_offset" : { "value" : -86400 } },
                  "identifier" : "[node:title] [list-item:value]",
                  "param_node" : [ "node" ],
                  "param_user" : [ "node:field-student" ]
                }
              }
            ]
          }
        }
      ]
    }
  }');
  return $items;
}
