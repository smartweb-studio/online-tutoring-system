<?php
/**
 * @file
 * content_type_lesson_plan.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function content_type_lesson_plan_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-lesson_plan-field_date_from'
  $field_instances['node-lesson_plan-field_date_from'] = array(
    'bundle' => 'lesson_plan',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_date_from',
    'label' => 'Date',
    'required' => 1,
    'settings' => array(
      'date_popup_timepicker' => array(
        'timepicker' => array(
          'timepicker_options' => array(
            'amPmText' => array(
              0 => 'AM',
              1 => 'PM',
            ),
            'closeButtonText' => 'Done',
            'defaultTime' => 'now',
            'deselectButtonText' => 'Deselect',
            'hourText' => 'Hour',
            'hours' => array(
              'ends' => 23,
              'starts' => 0,
            ),
            'minuteText' => 'Minute',
            'minutes' => array(
              'ends' => 55,
              'interval' => 5,
              'starts' => 0,
            ),
            'nowButtonText' => 'Now',
            'periodSeparator' => ' ',
            'rows' => 4,
            'showCloseButton' => FALSE,
            'showDeselectButton' => FALSE,
            'showHours' => TRUE,
            'showLeadingZero' => TRUE,
            'showMinutes' => TRUE,
            'showMinutesLeadingZero' => TRUE,
            'showNowButton' => FALSE,
            'showOn' => 'focus',
            'showPeriod' => FALSE,
            'showPeriodLabels' => TRUE,
            'timeSeparator' => ':',
          ),
        ),
      ),
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'restrictions' => array(
        'allowed_values' => array(
          'type' => 'weekdays',
          'weekdays' => array(
            'SU' => 'SU',
          ),
        ),
      ),
      'restrictions2' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(),
          'weekdays_host_entity' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
          'weekdays_product_display' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'Y-m-d H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'repeat_collapsed' => 0,
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-lesson_plan-field_key_tasks'
  $field_instances['node-lesson_plan-field_key_tasks'] = array(
    'bundle' => 'lesson_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_key_tasks',
    'label' => 'Key tasks',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-lesson_plan-field_lesson_plan_student'
  $field_instances['node-lesson_plan-field_lesson_plan_student'] = array(
    'bundle' => 'lesson_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_lesson_plan_student',
    'label' => 'Student',
    'required' => 1,
    'settings' => array(
      'nodeaccess_userreference' => array(
        'all' => array(
          'view' => 0,
        ),
        'all_published' => 0,
        'author' => array(
          'delete' => 0,
          'update' => 0,
          'view' => 0,
        ),
        'author_published' => 0,
        'create' => array(
          'article' => 0,
          'lesson' => 0,
          'lesson_plan' => 0,
          'page' => 0,
        ),
        'priority' => 0,
        'referenced' => array(
          'delete' => 0,
          'deny_delete' => 0,
          'deny_update' => 0,
          'deny_view' => 0,
          'update' => 0,
          'view' => 0,
        ),
        'referenced_published' => 0,
        'unused' => 0,
        'views' => array(
          'view' => '',
          'view_args' => '',
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => 1,
      ),
      'type' => 'options_select',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-lesson_plan-field_notes'
  $field_instances['node-lesson_plan-field_notes'] = array(
    'bundle' => 'lesson_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_notes',
    'label' => 'Notes',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-lesson_plan-field_objectives'
  $field_instances['node-lesson_plan-field_objectives'] = array(
    'bundle' => 'lesson_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_objectives',
    'label' => 'Objectives',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-lesson_plan-field_outcome'
  $field_instances['node-lesson_plan-field_outcome'] = array(
    'bundle' => 'lesson_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_outcome',
    'label' => 'Outcome',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-lesson_plan-field_resources'
  $field_instances['node-lesson_plan-field_resources'] = array(
    'bundle' => 'lesson_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_resources',
    'label' => 'Resources',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Date');
  t('Key tasks');
  t('Notes');
  t('Objectives');
  t('Outcome');
  t('Resources');
  t('Student');

  return $field_instances;
}
