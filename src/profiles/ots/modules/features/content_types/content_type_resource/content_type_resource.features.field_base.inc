<?php
/**
 * @file
 * content_type_resource.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function content_type_resource_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_category'
  $field_bases['field_category'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_category',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Podcast' => 'Podcast',
        'TV' => 'TV',
        'Movies' => 'Movies',
        'Books' => 'Books',
        'Magazines' => 'Magazines',
        'Music' => 'Music',
        'Text Book' => 'Text Book',
        'Flash Cards' => 'Flash Cards',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_completed'
  $field_bases['field_completed'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_completed',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'False',
        1 => 'True',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_boolean',
  );

  // Exported field_base: 'field_description'
  $field_bases['field_description'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_description',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_link_to_a_resource'
  $field_bases['field_link_to_a_resource'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_link_to_a_resource',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'profile2_private' => FALSE,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'translatable' => 0,
    'type' => 'link_field',
  );

  // Exported field_base: 'field_time'
  $field_bases['field_time'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_time',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        '0h 00m' => '0h 00m',
        '0h 05m' => '0h 05m',
        '0h 10m' => '0h 10m',
        '0h 15m' => '0h 15m',
        '0h 20m' => '0h 20m',
        '0h 25m' => '0h 25m',
        '0h 30m' => '0h 30m',
        '0h 35m' => '0h 35m',
        '0h 40m' => '0h 40m',
        '0h 45m' => '0h 45m',
        '0h 50m' => '0h 50m',
        '0h 55m' => '0h 55m',
        '1h 00m' => '1h 00m',
        '1h 05m' => '1h 05m',
        '1h 10m' => '1h 10m',
        '1h 15m' => '1h 15m',
        '1h 20m' => '1h 20m',
        '1h 25m' => '1h 25m',
        '1h 30m' => '1h 30m',
        '1h 35m' => '1h 35m',
        '1h 40m' => '1h 40m',
        '1h 45m' => '1h 45m',
        '1h 50m' => '1h 50m',
        '1h 55m' => '1h 55m',
        '2h 00m' => '2h 00m',
        '2h 05m' => '2h 05m',
        '2h 10m' => '2h 10m',
        '2h 15m' => '2h 15m',
        '2h 20m' => '2h 20m',
        '2h 25m' => '2h 25m',
        '2h 30m' => '2h 30m',
        '2h 35m' => '2h 35m',
        '2h 40m' => '2h 40m',
        '2h 45m' => '2h 45m',
        '2h 50m' => '2h 50m',
        '2h 55m' => '2h 55m',
        '3h 00m' => '3h 00m',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
