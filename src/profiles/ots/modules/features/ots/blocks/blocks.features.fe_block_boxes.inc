<?php
/**
 * @file
 * blocks.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function blocks_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'info-bottom';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'info_bottom';
  $fe_block_boxes->body = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fermentum ipsum id metus sollicitudin, lobortis tempus felis blandit. Phasellus faucibus lectus ac vestibulum porta. Quisque dictum ultricies sagittis. Phasellus nec aliquam metus. Nunc pharetra leo sit amet porttitor dapibus. Nulla eget justo in dolor consectetur imperdiet nec vitae eros. Curabitur dignissim eros quis orci scelerisque dapibus. Praesent vel efficitur elit. Nunc vel nibh velit. Curabitur tristique tempus diam et cursus. Etiam rutrum tellus sit amet urna vestibulum, non consequat leo imperdiet. Integer non congue purus. Aenean tempor bibendum magna a pretium. Nunc semper augue congue tellus sagittis dapibus. Cras et velit lacinia, iaculis urna quis, eleifend odio.</p>

<p>Phasellus congue magna vel laoreet efficitur. Curabitur est nunc, lobortis sit amet nulla sit amet, cursus feugiat est. Nullam massa nisi, faucibus nec tincidunt id, rutrum vitae est. Sed venenatis interdum magna, sed dictum justo tempus nec. Donec rutrum urna sed porta hendrerit. Nam at lectus eget odio maximus vulputate sed id quam. Donec iaculis accumsan convallis. Aliquam eget purus sit amet sem posuere fermentum id quis justo.</p>
';

  $export['info_bottom'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'info-center-left';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'info_center_left';
  $fe_block_boxes->body = '<p>The Cambridge Model stands for exellence in learning etc...&nbsp;The Cambridge Model stands for exellence in learning etc...&nbsp;The Cambridge Model stands for exellence in learning etc...&nbsp;</p>
';

  $export['info_center_left'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'info-center-right';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'info_center_right';
  $fe_block_boxes->body = '<p>The Cambridge Model stands for exellence in learning etc...&nbsp;The Cambridge Model stands for exellence in learning etc...&nbsp;The Cambridge Model stands for exellence in learning etc...&nbsp;</p>
';

  $export['info_center_right'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'info-left';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'info_left';
  $fe_block_boxes->body = '<p>The Cambridge Model stands for exellence in learning etc...&nbsp;The Cambridge Model stands for exellence in learning etc...&nbsp;The Cambridge Model stands for exellence in learning etc...&nbsp;</p>
';

  $export['info_left'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'info-right';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'info_right';
  $fe_block_boxes->body = '<p>The Cambridge Model stands for exellence in learning etc...&nbsp;The Cambridge Model stands for exellence in learning etc...&nbsp;The Cambridge Model stands for exellence in learning etc...&nbsp;</p>
';

  $export['info_right'] = $fe_block_boxes;

  return $export;
}
