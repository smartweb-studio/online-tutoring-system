<?php
/**
 * @file
 * blocks.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function blocks_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-info_bottom'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'info_bottom',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => 'info bottom',
    'visibility' => 0,
  );

  $export['block-info_center_left'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'info_center_left',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => 'Fast',
    'visibility' => 0,
  );

  $export['block-info_center_right'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'info_center_right',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => 'Best',
    'visibility' => 0,
  );

  $export['block-info_left'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'info_left',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => 'English learning',
    'visibility' => 0,
  );

  $export['block-info_right'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'info_right',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => 'Individual',
    'visibility' => 0,
  );

  $export['datepicker_block-0'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 0,
    'module' => 'datepicker_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['jump_menu-jump_menu-m_menu-admin-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'jump_menu-m_menu-admin-menu',
    'module' => 'jump_menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'administrator' => 6,
    ),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['jump_menu-jump_menu-m_menu-student-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'jump_menu-m_menu-student-menu',
    'module' => 'jump_menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'student' => 4,
    ),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['jump_menu-jump_menu-m_menu-tutor-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'jump_menu-m_menu-tutor-menu',
    'module' => 'jump_menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'tutor' => 5,
    ),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-admin-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-admin-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-resources-standard-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-resources-standard-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => 'dashboard/resources
dashboard/students/*/resources',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => 'after_content',
        'status' => 1,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['menu-menu-sign-in'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-sign-in',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'anonymous user' => 1,
    ),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => 'header_menu',
        'status' => 1,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['menu-menu-student-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-student-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-tutor-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-tutor-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => 'dashboard/students/*/resource',
    'roles' => array(
      'administrator' => 6,
      'tutor' => 5,
    ),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['ots_blocks-ots_resource'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'ots_resource',
    'module' => 'ots_blocks',
    'node_types' => array(),
    'pages' => 'dashboard/students/*/resources',
    'roles' => array(
      'administrator' => 6,
      'tutor' => 5,
    ),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => 'after_content',
        'status' => 1,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => 'dashboard/students/*/resource',
    'roles' => array(
      'administrator' => 6,
      'tutor' => 5,
    ),
    'themes' => array(
      'adminimal' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['menu-menu-tutor-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-tutor-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => 'dashboard/students/*/resource',
    'roles' => array(
      'administrator' => 6,
      'tutor' => 5,
    ),
    'themes' => array(
      'adminimal' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-meet_the_tutor-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'meet_the_tutor-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'meet-the-tutor',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => -12,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-students-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'students-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-tutor-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'tutor-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'goodnex' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'goodnex',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
