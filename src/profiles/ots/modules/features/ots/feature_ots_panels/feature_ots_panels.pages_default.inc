<?php
// @codingStandardsIgnoreFile
/**
 * @file
 * feature_ots_panels.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function feature_ots_panels_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template__anonymous';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'anonymous',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'anonymous',
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 1,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'threecol_25_50_25_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '85c48947-393f-407d-bb83-fe1c68789150';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-a0e915c7-543d-4b22-9f81-4161194af808';
    $pane->panel = 'middle';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a0e915c7-543d-4b22-9f81-4161194af808';
    $display->content['new-a0e915c7-543d-4b22-9f81-4161194af808'] = $pane;
    $display->panels['middle'][0] = 'new-a0e915c7-543d-4b22-9f81-4161194af808';
    $pane = new stdClass();
    $pane->pid = 'new-48b63b84-5024-4d52-b5fa-6d87f5765ed4';
    $pane->panel = 'top';
    $pane->type = 'page_logo';
    $pane->subtype = 'page_logo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '48b63b84-5024-4d52-b5fa-6d87f5765ed4';
    $display->content['new-48b63b84-5024-4d52-b5fa-6d87f5765ed4'] = $pane;
    $display->panels['top'][0] = 'new-48b63b84-5024-4d52-b5fa-6d87f5765ed4';
    $pane = new stdClass();
    $pane->pid = 'new-c5165b41-7e99-43eb-8bfe-4f07edadd04b';
    $pane->panel = 'top';
    $pane->type = 'page_site_name';
    $pane->subtype = 'page_site_name';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'linked' => 1,
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c5165b41-7e99-43eb-8bfe-4f07edadd04b';
    $display->content['new-c5165b41-7e99-43eb-8bfe-4f07edadd04b'] = $pane;
    $display->panels['top'][1] = 'new-c5165b41-7e99-43eb-8bfe-4f07edadd04b';
    $pane = new stdClass();
    $pane->pid = 'new-2b426a08-bdf3-4e43-a4ec-dfc8058ee448';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '2b426a08-bdf3-4e43-a4ec-dfc8058ee448';
    $display->content['new-2b426a08-bdf3-4e43-a4ec-dfc8058ee448'] = $pane;
    $display->panels['top'][2] = 'new-2b426a08-bdf3-4e43-a4ec-dfc8058ee448';
    $pane = new stdClass();
    $pane->pid = 'new-f3ba9fa9-3155-4639-ad07-adca0a39a084';
    $pane->panel = 'top';
    $pane->type = 'page_messages';
    $pane->subtype = 'page_messages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'f3ba9fa9-3155-4639-ad07-adca0a39a084';
    $display->content['new-f3ba9fa9-3155-4639-ad07-adca0a39a084'] = $pane;
    $display->panels['top'][3] = 'new-f3ba9fa9-3155-4639-ad07-adca0a39a084';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['site_template__anonymous'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template__authenticated';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'authenticated',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'authenticated',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 2,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'threecol_25_50_25_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'middle' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '47bf7c46-5d90-4e2d-a922-d55612e32570';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-e71df15f-037f-4c21-937f-fc272b412566';
    $pane->panel = 'middle';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e71df15f-037f-4c21-937f-fc272b412566';
    $display->content['new-e71df15f-037f-4c21-937f-fc272b412566'] = $pane;
    $display->panels['middle'][0] = 'new-e71df15f-037f-4c21-937f-fc272b412566';
    $pane = new stdClass();
    $pane->pid = 'new-42e2838a-0bdc-4324-94c1-f1912ec603bc';
    $pane->panel = 'top';
    $pane->type = 'page_logo';
    $pane->subtype = 'page_logo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '42e2838a-0bdc-4324-94c1-f1912ec603bc';
    $display->content['new-42e2838a-0bdc-4324-94c1-f1912ec603bc'] = $pane;
    $display->panels['top'][0] = 'new-42e2838a-0bdc-4324-94c1-f1912ec603bc';
    $pane = new stdClass();
    $pane->pid = 'new-9bdbc2a2-7876-47a5-9e0d-0b3289f516d4';
    $pane->panel = 'top';
    $pane->type = 'page_site_name';
    $pane->subtype = 'page_site_name';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'linked' => 1,
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9bdbc2a2-7876-47a5-9e0d-0b3289f516d4';
    $display->content['new-9bdbc2a2-7876-47a5-9e0d-0b3289f516d4'] = $pane;
    $display->panels['top'][1] = 'new-9bdbc2a2-7876-47a5-9e0d-0b3289f516d4';
    $pane = new stdClass();
    $pane->pid = 'new-3e39f971-b78c-42d6-bd50-f9e7c0f497e8';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'jump_menu-jump_menu-m_menu-admin-menu';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 6,
              1 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '3e39f971-b78c-42d6-bd50-f9e7c0f497e8';
    $display->content['new-3e39f971-b78c-42d6-bd50-f9e7c0f497e8'] = $pane;
    $display->panels['top'][2] = 'new-3e39f971-b78c-42d6-bd50-f9e7c0f497e8';
    $pane = new stdClass();
    $pane->pid = 'new-37ff626c-8cce-4b76-8b59-1fb704e9bacb';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'jump_menu-jump_menu-m_menu-student-menu';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 4,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '37ff626c-8cce-4b76-8b59-1fb704e9bacb';
    $display->content['new-37ff626c-8cce-4b76-8b59-1fb704e9bacb'] = $pane;
    $display->panels['top'][3] = 'new-37ff626c-8cce-4b76-8b59-1fb704e9bacb';
    $pane = new stdClass();
    $pane->pid = 'new-2edbba48-ad99-4ec0-be57-870ac0109839';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'jump_menu-jump_menu-m_menu-tutor-menu';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 5,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '2edbba48-ad99-4ec0-be57-870ac0109839';
    $display->content['new-2edbba48-ad99-4ec0-be57-870ac0109839'] = $pane;
    $display->panels['top'][4] = 'new-2edbba48-ad99-4ec0-be57-870ac0109839';
    $pane = new stdClass();
    $pane->pid = 'new-1f9d7c15-6e76-4039-bf4b-4777077c61b2';
    $pane->panel = 'top';
    $pane->type = 'token';
    $pane->subtype = 'user:name';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'sanitize' => 1,
      'context' => 'logged-in-user',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '1f9d7c15-6e76-4039-bf4b-4777077c61b2';
    $display->content['new-1f9d7c15-6e76-4039-bf4b-4777077c61b2'] = $pane;
    $display->panels['top'][5] = 'new-1f9d7c15-6e76-4039-bf4b-4777077c61b2';
    $pane = new stdClass();
    $pane->pid = 'new-4753e5c5-f600-4894-86ec-1fdfc2e8ad23';
    $pane->panel = 'top';
    $pane->type = 'pane_messages';
    $pane->subtype = 'pane_messages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = '4753e5c5-f600-4894-86ec-1fdfc2e8ad23';
    $display->content['new-4753e5c5-f600-4894-86ec-1fdfc2e8ad23'] = $pane;
    $display->panels['top'][6] = 'new-4753e5c5-f600-4894-86ec-1fdfc2e8ad23';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-42e2838a-0bdc-4324-94c1-f1912ec603bc';
  $handler->conf['display'] = $display;
  $export['site_template__authenticated'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'user_view__default_profile';
  $handler->task = 'user_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 3;
  $handler->conf = array(
    'title' => 'Default profile',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'default_profile',
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '3a4449e7-e5f5-4a33-9b73-189a3b12beba';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-44bd51c2-4a42-4c6e-a1e6-77ce1ff67028';
    $pane->panel = 'center';
    $pane->type = 'user_profile';
    $pane->subtype = 'user_profile';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:user_1',
      'override_title' => 1,
      'override_title_text' => '%user:name',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '44bd51c2-4a42-4c6e-a1e6-77ce1ff67028';
    $display->content['new-44bd51c2-4a42-4c6e-a1e6-77ce1ff67028'] = $pane;
    $display->panels['center'][0] = 'new-44bd51c2-4a42-4c6e-a1e6-77ce1ff67028';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['user_view__default_profile'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function feature_ots_panels_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'homepage';
  $page->task = 'page';
  $page->admin_title = 'homepage';
  $page->admin_description = '';
  $page->path = 'home';
  $page->access = array(
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'none',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_homepage__panel_context_252d2b29-e610-4c2b-b70a-24c83926c946';
  $handler->task = 'page';
  $handler->subtask = 'homepage';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '4d7ef316-c152-4c5d-a881-8953cff7bda9';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f3ec6dd4-77c6-4729-9cf5-d1a5becd9d07';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'block-1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'English learning',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f3ec6dd4-77c6-4729-9cf5-d1a5becd9d07';
    $display->content['new-f3ec6dd4-77c6-4729-9cf5-d1a5becd9d07'] = $pane;
    $display->panels['center'][0] = 'new-f3ec6dd4-77c6-4729-9cf5-d1a5becd9d07';
    $pane = new stdClass();
    $pane->pid = 'new-17d0620a-6cc6-4ea6-84eb-1b649daf3d1e';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'block-2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Fast',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '17d0620a-6cc6-4ea6-84eb-1b649daf3d1e';
    $display->content['new-17d0620a-6cc6-4ea6-84eb-1b649daf3d1e'] = $pane;
    $display->panels['center'][1] = 'new-17d0620a-6cc6-4ea6-84eb-1b649daf3d1e';
    $pane = new stdClass();
    $pane->pid = 'new-3d5bd65c-06ba-4ad2-91c7-ec8d055e1984';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'block-3';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Best',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '3d5bd65c-06ba-4ad2-91c7-ec8d055e1984';
    $display->content['new-3d5bd65c-06ba-4ad2-91c7-ec8d055e1984'] = $pane;
    $display->panels['center'][2] = 'new-3d5bd65c-06ba-4ad2-91c7-ec8d055e1984';
    $pane = new stdClass();
    $pane->pid = 'new-eeac6914-fd29-4b64-b22a-1a8a99278e58';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'block-4';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Individual',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'eeac6914-fd29-4b64-b22a-1a8a99278e58';
    $display->content['new-eeac6914-fd29-4b64-b22a-1a8a99278e58'] = $pane;
    $display->panels['center'][3] = 'new-eeac6914-fd29-4b64-b22a-1a8a99278e58';
    $pane = new stdClass();
    $pane->pid = 'new-8ead5734-0be6-4789-85ba-2a618950f2df';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'block-5';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '8ead5734-0be6-4789-85ba-2a618950f2df';
    $display->content['new-8ead5734-0be6-4789-85ba-2a618950f2df'] = $pane;
    $display->panels['center'][4] = 'new-8ead5734-0be6-4789-85ba-2a618950f2df';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['homepage'] = $page;

  return $pages;

}
