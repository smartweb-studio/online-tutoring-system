<?php
/**
 * @file
 * feature_ots_user_settings.default_field_validation_rules.inc
 */

/**
 * Implements hook_default_field_validation_rule().
 */
function feature_ots_user_settings_default_field_validation_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Required interview times';
  $rule->name = 'required_interview_times';
  $rule->field_name = 'field_best_time_to_be_interviewe';
  $rule->col = 'value';
  $rule->entity_type = 'profile2';
  $rule->bundle = 'main';
  $rule->validator = 'field_validation_someofseveral_validator';
  $rule->settings = array(
    'data' => 'Interview times',
    'required_fields' => '2',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      4 => 0,
      5 => 0,
      6 => 0,
      3 => 0,
    ),
    'errors' => 1,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'You should enter at least two best times to be interviewed!';
  $export['required_interview_times'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Required tutoring times';
  $rule->name = 'required_tutoring_times';
  $rule->field_name = 'field_best_times_to_have_lessons';
  $rule->col = 'value';
  $rule->entity_type = 'profile2';
  $rule->bundle = 'main';
  $rule->validator = 'field_validation_someofseveral_validator';
  $rule->settings = array(
    'data' => 'Lessons times',
    'required_fields' => '2',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      4 => 0,
      5 => 0,
      6 => 0,
      3 => 0,
    ),
    'errors' => 1,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'You should enter at least two best times to have lessons!';
  $export['required_tutoring_times'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Symbol Lengh for School Name';
  $rule->name = 'symbol_lengh_for_school_name';
  $rule->field_name = 'field_school_education_instituti';
  $rule->col = 'value';
  $rule->entity_type = 'profile2';
  $rule->bundle = 'main';
  $rule->validator = 'field_validation_length_validator';
  $rule->settings = array(
    'min' => '0',
    'max' => '255',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      4 => 0,
      5 => 0,
      6 => 0,
      3 => 0,
    ),
    'errors' => 1,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'Maximum available length is 255';
  $export['symbol_lengh_for_school_name'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Symbol Lengh for Text Book';
  $rule->name = 'symbol_lengh_for_text_book';
  $rule->field_name = 'field_text_book_used_in_school';
  $rule->col = 'value';
  $rule->entity_type = 'profile2';
  $rule->bundle = 'main';
  $rule->validator = 'field_validation_length_validator';
  $rule->settings = array(
    'min' => '0',
    'max' => '255',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      4 => 0,
      5 => 0,
      6 => 0,
      3 => 0,
    ),
    'errors' => 1,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'Maximum available length is 255';
  $export['symbol_lengh_for_text_book'] = $rule;

  return $export;
}
