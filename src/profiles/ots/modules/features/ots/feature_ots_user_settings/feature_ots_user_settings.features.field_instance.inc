<?php
/**
 * @file
 * feature_ots_user_settings.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function feature_ots_user_settings_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'profile2-main-field_address'
  $field_instances['profile2-main-field_address'] = array(
    'bundle' => 'main',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'addressfield',
        'settings' => array(
          'format_handlers' => array(
            0 => 'address',
          ),
          'use_widget_handlers' => 1,
        ),
        'type' => 'addressfield_default',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_address',
    'label' => 'Address',
    'required' => 1,
    'settings' => array(
      'date_popup_timepicker' => array(
        'timepicker' => array(
          'timepicker_options' => array(
            'amPmText' => array(
              0 => 'AM',
              1 => 'PM',
            ),
            'closeButtonText' => 'Done',
            'defaultTime' => 'now',
            'deselectButtonText' => 'Deselect',
            'hourText' => 'Hour',
            'hours' => array(
              'ends' => 23,
              'starts' => 0,
            ),
            'minuteText' => 'Minute',
            'minutes' => array(
              'ends' => 55,
              'interval' => 5,
              'starts' => 0,
            ),
            'nowButtonText' => 'Now',
            'periodSeparator' => ' ',
            'rows' => 4,
            'showCloseButton' => FALSE,
            'showDeselectButton' => FALSE,
            'showHours' => TRUE,
            'showLeadingZero' => TRUE,
            'showMinutes' => TRUE,
            'showMinutesLeadingZero' => TRUE,
            'showNowButton' => FALSE,
            'showOn' => 'focus',
            'showPeriod' => FALSE,
            'showPeriodLabels' => TRUE,
            'timeSeparator' => ':',
          ),
        ),
      ),
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'addressfield',
      'settings' => array(
        'available_countries' => array(),
        'default_country' => 'site_default',
        'format_handlers' => array(
          'address' => 'address',
          'phone' => 0,
          'address-hide-postal-code' => 0,
          'address-hide-street' => 0,
          'address-hide-country' => 0,
          'organisation' => 0,
          'name-full' => 0,
          'name-oneline' => 0,
          'address-optional' => 0,
        ),
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'phone_number_fields' => array(
          'extension' => 0,
          'fax' => 0,
          'mobile' => 0,
          'phone' => 0,
        ),
        'text_parts' => array(),
        'year_range' => '-0:+5',
      ),
      'type' => 'addressfield_standard',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'profile2-main-field_best_time_to_be_interviewe'
  $field_instances['profile2-main-field_best_time_to_be_interviewe'] = array(
    'bundle' => 'main',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 16,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_best_time_to_be_interviewe',
    'label' => 'Best time to be interviewed',
    'required' => 1,
    'settings' => array(
      'date_popup_timepicker' => array(
        'timepicker' => array(
          'timepicker_options' => array(
            'amPmText' => array(
              0 => 'AM',
              1 => 'PM',
            ),
            'closeButtonText' => 'Done',
            'defaultTime' => 'now',
            'deselectButtonText' => 'Deselect',
            'hourText' => 'Hour',
            'hours' => array(
              'ends' => 23,
              'starts' => 0,
            ),
            'minuteText' => 'Minute',
            'minutes' => array(
              'ends' => 55,
              'interval' => 5,
              'starts' => 0,
            ),
            'nowButtonText' => 'Now',
            'periodSeparator' => ' ',
            'rows' => 4,
            'showCloseButton' => FALSE,
            'showDeselectButton' => FALSE,
            'showHours' => TRUE,
            'showLeadingZero' => TRUE,
            'showMinutes' => TRUE,
            'showMinutesLeadingZero' => TRUE,
            'showNowButton' => FALSE,
            'showOn' => 'focus',
            'showPeriod' => FALSE,
            'showPeriodLabels' => TRUE,
            'timeSeparator' => ':',
          ),
        ),
      ),
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'restrictions' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(),
          'weekdays_host_entity' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
          'weekdays_product_display' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
        ),
      ),
      'restrictions2' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(),
          'weekdays_host_entity' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
          'weekdays_product_display' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'Y-m-d H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'repeat_collapsed' => 0,
        'text_parts' => array(),
        'year_range' => '-0:+5',
      ),
      'type' => 'date_popup',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'profile2-main-field_best_times_to_have_lessons'
  $field_instances['profile2-main-field_best_times_to_have_lessons'] = array(
    'bundle' => 'main',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 17,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_best_times_to_have_lessons',
    'label' => 'Best times to have lessons',
    'required' => 1,
    'settings' => array(
      'date_popup_timepicker' => array(
        'timepicker' => array(
          'timepicker_options' => array(
            'amPmText' => array(
              0 => 'AM',
              1 => 'PM',
            ),
            'closeButtonText' => 'Done',
            'defaultTime' => 'now',
            'deselectButtonText' => 'Deselect',
            'hourText' => 'Hour',
            'hours' => array(
              'ends' => 23,
              'starts' => 0,
            ),
            'minuteText' => 'Minute',
            'minutes' => array(
              'ends' => 55,
              'interval' => 5,
              'starts' => 0,
            ),
            'nowButtonText' => 'Now',
            'periodSeparator' => ' ',
            'rows' => 4,
            'showCloseButton' => FALSE,
            'showDeselectButton' => FALSE,
            'showHours' => TRUE,
            'showLeadingZero' => TRUE,
            'showMinutes' => TRUE,
            'showMinutesLeadingZero' => TRUE,
            'showNowButton' => FALSE,
            'showOn' => 'focus',
            'showPeriod' => FALSE,
            'showPeriodLabels' => TRUE,
            'timeSeparator' => ':',
          ),
        ),
      ),
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'restrictions' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(),
          'weekdays_host_entity' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
          'weekdays_product_display' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
        ),
      ),
      'restrictions2' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(),
          'weekdays_host_entity' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
          'weekdays_product_display' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'Y-m-d H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'repeat_collapsed' => 0,
        'text_parts' => array(),
        'year_range' => '-0:+5',
      ),
      'type' => 'date_popup',
      'weight' => 11,
    ),
  );

  // Exported field_instance: 'profile2-main-field_date_of_birth'
  $field_instances['profile2-main-field_date_of_birth'] = array(
    'bundle' => 'main',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_date_of_birth',
    'label' => 'Date of Birth',
    'required' => 1,
    'settings' => array(
      'date_popup_timepicker' => array(
        'timepicker' => array(
          'timepicker_options' => array(
            'amPmText' => array(
              0 => 'AM',
              1 => 'PM',
            ),
            'closeButtonText' => 'Done',
            'defaultTime' => 'now',
            'deselectButtonText' => 'Deselect',
            'hourText' => 'Hour',
            'hours' => array(
              'ends' => 23,
              'starts' => 0,
            ),
            'minuteText' => 'Minute',
            'minutes' => array(
              'ends' => 55,
              'interval' => 5,
              'starts' => 0,
            ),
            'nowButtonText' => 'Now',
            'periodSeparator' => ' ',
            'rows' => 4,
            'showCloseButton' => FALSE,
            'showDeselectButton' => FALSE,
            'showHours' => TRUE,
            'showLeadingZero' => TRUE,
            'showMinutes' => TRUE,
            'showMinutesLeadingZero' => TRUE,
            'showNowButton' => FALSE,
            'showOn' => 'focus',
            'showPeriod' => FALSE,
            'showPeriodLabels' => TRUE,
            'timeSeparator' => ':',
          ),
        ),
      ),
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'restrictions' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(),
          'weekdays_host_entity' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
          'weekdays_product_display' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
        ),
      ),
      'restrictions2' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(),
          'weekdays_host_entity' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
          'weekdays_product_display' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'Y-m-d H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'repeat_collapsed' => 0,
        'text_parts' => array(),
        'year_range' => '-50:-5',
      ),
      'type' => 'date_popup',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'profile2-main-field_first_name'
  $field_instances['profile2-main-field_first_name'] = array(
    'bundle' => 'main',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_first_name',
    'label' => 'First Name',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'profile2-main-field_gender'
  $field_instances['profile2-main-field_gender'] = array(
    'bundle' => 'main',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_gender',
    'label' => 'Gender',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => '',
      ),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'profile2-main-field_last_name'
  $field_instances['profile2-main-field_last_name'] = array(
    'bundle' => 'main',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_last_name',
    'label' => 'Last Name',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'profile2-main-field_phone_number'
  $field_instances['profile2-main-field_phone_number'] = array(
    'bundle' => 'main',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 18,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_phone_number',
    'label' => 'Phone Number',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 17,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'profile2-main-field_school_education_instituti'
  $field_instances['profile2-main-field_school_education_instituti'] = array(
    'bundle' => 'main',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_school_education_instituti',
    'label' => 'School/Education Institution',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'profile2-main-field_school_year'
  $field_instances['profile2-main-field_school_year'] = array(
    'bundle' => 'main',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_school_year',
    'label' => 'School Year',
    'required' => 1,
    'settings' => array(
      'date_popup_timepicker' => array(
        'timepicker' => array(
          'timepicker_options' => array(
            'amPmText' => array(
              0 => 'AM',
              1 => 'PM',
            ),
            'closeButtonText' => 'Done',
            'defaultTime' => 'now',
            'deselectButtonText' => 'Deselect',
            'hourText' => 'Hour',
            'hours' => array(
              'ends' => 23,
              'starts' => 0,
            ),
            'maxTime' => array(
              'hour' => '',
              'minute' => '',
            ),
            'minTime' => array(
              'hour' => '',
              'minute' => '',
            ),
            'minuteText' => 'Minute',
            'minutes' => array(
              'ends' => 55,
              'interval' => 5,
              'starts' => 0,
            ),
            'nowButtonText' => 'Now',
            'periodSeparator' => ' ',
            'rows' => 4,
            'showCloseButton' => 0,
            'showDeselectButton' => 0,
            'showHours' => 1,
            'showLeadingZero' => 1,
            'showMinutes' => 1,
            'showMinutesLeadingZero' => 1,
            'showNowButton' => 0,
            'showOn' => 'focus',
            'showPeriod' => 0,
            'showPeriodLabels' => 1,
            'timeSeparator' => ':',
          ),
        ),
      ),
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'restrictions' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(),
          'weekdays_host_entity' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
          'weekdays_product_display' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
        ),
      ),
      'restrictions2' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(),
          'weekdays_host_entity' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
          'weekdays_product_display' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'Y-m-d H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'repeat_collapsed' => 0,
        'text_parts' => array(),
        'year_range' => '-50:+5',
      ),
      'type' => 'date_select',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'profile2-main-field_text_book_used_in_school'
  $field_instances['profile2-main-field_text_book_used_in_school'] = array(
    'bundle' => 'main',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 15,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_text_book_used_in_school',
    'label' => 'Text books used in school',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'profile2-main-field_years_studied_english'
  $field_instances['profile2-main-field_years_studied_english'] = array(
    'bundle' => 'main',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 9,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_years_studied_english',
    'label' => 'Years studied English',
    'required' => 1,
    'settings' => array(
      'max' => 50,
      'min' => 1,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'profile2-tutor_profile-field_address'
  $field_instances['profile2-tutor_profile-field_address'] = array(
    'bundle' => 'tutor_profile',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'account' => array(
        'label' => 'above',
        'module' => 'addressfield',
        'settings' => array(
          'format_handlers' => array(
            0 => 'address',
          ),
          'use_widget_handlers' => 1,
        ),
        'type' => 'addressfield_default',
        'weight' => 7,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'addressfield',
        'settings' => array(
          'format_handlers' => array(
            0 => 'address',
          ),
          'use_widget_handlers' => 1,
        ),
        'type' => 'addressfield_default',
        'weight' => 7,
      ),
      'token' => array(
        'label' => 'above',
        'module' => 'addressfield',
        'settings' => array(
          'format_handlers' => array(
            0 => 'address',
          ),
          'use_widget_handlers' => 1,
        ),
        'type' => 'addressfield_default',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_address',
    'label' => 'Address',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'addressfield',
      'settings' => array(
        'available_countries' => array(),
        'default_country' => 'site_default',
        'format_handlers' => array(
          'address' => 'address',
          'phone' => 0,
          'address-hide-postal-code' => 0,
          'address-hide-street' => 0,
          'address-hide-country' => 0,
          'organisation' => 0,
          'name-full' => 0,
          'name-oneline' => 0,
          'address-optional' => 0,
        ),
        'phone_number_fields' => array(
          'extension' => 0,
          'fax' => 0,
          'mobile' => 0,
          'phone' => 0,
        ),
      ),
      'type' => 'addressfield_standard',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'profile2-tutor_profile-field_date_of_birth'
  $field_instances['profile2-tutor_profile-field_date_of_birth'] = array(
    'bundle' => 'tutor_profile',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'account' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 2,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 2,
      ),
      'token' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_date_of_birth',
    'label' => 'Date of Birth',
    'required' => 1,
    'settings' => array(
      'date_popup_timepicker' => array(
        'timepicker' => array(
          'timepicker_options' => array(
            'amPmText' => array(
              0 => 'AM',
              1 => 'PM',
            ),
            'closeButtonText' => 'Done',
            'defaultTime' => 'now',
            'deselectButtonText' => 'Deselect',
            'hourText' => 'Hour',
            'hours' => array(
              'ends' => 23,
              'starts' => 0,
            ),
            'minuteText' => 'Minute',
            'minutes' => array(
              'ends' => 55,
              'interval' => 5,
              'starts' => 0,
            ),
            'nowButtonText' => 'Now',
            'periodSeparator' => ' ',
            'rows' => 4,
            'showCloseButton' => FALSE,
            'showDeselectButton' => FALSE,
            'showHours' => TRUE,
            'showLeadingZero' => TRUE,
            'showMinutes' => TRUE,
            'showMinutesLeadingZero' => TRUE,
            'showNowButton' => FALSE,
            'showOn' => 'focus',
            'showPeriod' => FALSE,
            'showPeriodLabels' => TRUE,
            'timeSeparator' => ':',
          ),
        ),
      ),
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'restrictions' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(),
          'weekdays_host_entity' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
          'weekdays_product_display' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
        ),
      ),
      'restrictions2' => array(
        'allowed_values' => array(
          'type' => 'disabled',
          'weekdays' => array(),
          'weekdays_host_entity' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
          'weekdays_product_display' => array(
            'field' => NULL,
            'reversed' => FALSE,
          ),
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'Y-m-d H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'repeat_collapsed' => 0,
        'text_parts' => array(),
        'year_range' => '-50:-5',
      ),
      'type' => 'date_popup',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'profile2-tutor_profile-field_first_name'
  $field_instances['profile2-tutor_profile-field_first_name'] = array(
    'bundle' => 'tutor_profile',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'account' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'token' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_first_name',
    'label' => 'First Name',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'profile2-tutor_profile-field_gender'
  $field_instances['profile2-tutor_profile-field_gender'] = array(
    'bundle' => 'tutor_profile',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'account' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 6,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 6,
      ),
      'token' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_gender',
    'label' => 'Gender',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => '',
      ),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'profile2-tutor_profile-field_last_name'
  $field_instances['profile2-tutor_profile-field_last_name'] = array(
    'bundle' => 'tutor_profile',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'account' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'token' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_last_name',
    'label' => 'Last Name',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'profile2-tutor_profile-field_phone_number'
  $field_instances['profile2-tutor_profile-field_phone_number'] = array(
    'bundle' => 'tutor_profile',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 9,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_phone_number',
    'label' => 'Phone Number',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 17,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'profile2-tutor_profile-field_students'
  $field_instances['profile2-tutor_profile-field_students'] = array(
    'bundle' => 'tutor_profile',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'profile2',
    'field_name' => 'field_students',
    'label' => 'Students',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => 1,
      ),
      'type' => 'options_select',
      'weight' => 6,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Address');
  t('Best time to be interviewed');
  t('Best times to have lessons');
  t('Date of Birth');
  t('First Name');
  t('Gender');
  t('Last Name');
  t('Phone Number');
  t('School Year');
  t('School/Education Institution');
  t('Students');
  t('Text books used in school');
  t('Years studied English');

  return $field_instances;
}
