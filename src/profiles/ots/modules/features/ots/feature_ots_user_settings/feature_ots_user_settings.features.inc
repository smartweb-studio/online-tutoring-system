<?php
/**
 * @file
 * feature_ots_user_settings.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_ots_user_settings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_validation" && $api == "default_field_validation_rules") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_default_profile2_type().
 */
function feature_ots_user_settings_default_profile2_type() {
  $items = array();
  $items['main'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "main",
    "label" : "Student",
    "weight" : "0",
    "data" : { "registration" : 0 }
  }');
  $items['tutor_profile'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "tutor_profile",
    "label" : "Tutor",
    "weight" : "0",
    "data" : { "registration" : 0 }
  }');
  return $items;
}
