<?php
/**
 * @file
 * feature_ots_user_settings.features.roles_permissions.inc
 */

/**
 * Implements hook_default_roles_permissions().
 */
function feature_ots_user_settings_default_roles_permissions() {
  $roles = array();

  // Exported role: curator
  $roles['curator'] = array(
    'name' => 'curator',
    'weight' => 4,
    'permissions' => array(
      'view files' => TRUE,
    ),
  );

  // Exported role: student
  $roles['student'] = array(
    'name' => 'student',
    'weight' => 2,
    'permissions' => array(
      'access content' => TRUE,
      'participate in workflow' => TRUE,
      'show workflow state form' => TRUE,
      'view files' => TRUE,
    ),
  );

  // Exported role: tutor
  $roles['tutor'] = array(
    'name' => 'tutor',
    'weight' => 3,
    'permissions' => array(
      'access content' => TRUE,
      'access user profiles' => TRUE,
      'create lesson content' => TRUE,
      'create resource content' => TRUE,
      'edit own resource content' => TRUE,
      'create lesson_plan content' => TRUE,
      'edit own lesson content' => TRUE,
      'edit own lesson_plan content' => TRUE,
      'participate in workflow' => TRUE,
      'show workflow state form' => TRUE,
      'view files' => TRUE,
    ),
  );

  return $roles;
}
