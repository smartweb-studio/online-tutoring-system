<?php
/**
 * @file
 * feature_ots_user_settings.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function feature_ots_user_settings_user_default_roles() {
  $roles = array();

  // Exported role: curator.
  $roles['curator'] = array(
    'name' => 'curator',
    'weight' => 4,
  );

  // Exported role: student.
  $roles['student'] = array(
    'name' => 'student',
    'weight' => 2,
  );

  // Exported role: tutor.
  $roles['tutor'] = array(
    'name' => 'tutor',
    'weight' => 3,
  );

  return $roles;
}
