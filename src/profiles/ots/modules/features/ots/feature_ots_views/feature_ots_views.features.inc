<?php
/**
 * @file
 * feature_ots_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function feature_ots_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
