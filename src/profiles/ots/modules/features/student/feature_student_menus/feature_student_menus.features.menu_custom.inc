<?php
/**
 * @file
 * feature_student_menus.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function feature_student_menus_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-resources-standard-menu.
  $menus['menu-resources-standard-menu'] = array(
    'menu_name' => 'menu-resources-standard-menu',
    'title' => 'Standard',
    'description' => '',
  );
  // Exported menu: menu-student-menu.
  $menus['menu-student-menu'] = array(
    'menu_name' => 'menu-student-menu',
    'title' => 'role student menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Standard');
  t('role student menu');


  return $menus;
}
