<?php
/**
 * @file
 * feature_student_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function feature_student_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-resources-standard-menu_dictionary:<front>
  $menu_links['menu-resources-standard-menu_dictionary:<front>'] = array(
    'menu_name' => 'menu-resources-standard-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Dictionary',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'profiles/ots/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-resources-standard-menu_dictionary:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-resources-standard-menu_flash-card-app:<front>
  $menu_links['menu-resources-standard-menu_flash-card-app:<front>'] = array(
    'menu_name' => 'menu-resources-standard-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Flash Card App',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'profiles/ots/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-resources-standard-menu_flash-card-app:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-resources-standard-menu_text-book:<front>
  $menu_links['menu-resources-standard-menu_text-book:<front>'] = array(
    'menu_name' => 'menu-resources-standard-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Text Book',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'profiles/ots/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-resources-standard-menu_text-book:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-student-menu_account:<front>
  $menu_links['menu-student-menu_account:<front>'] = array(
    'menu_name' => 'menu-student-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Account',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'sites/all/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-student-menu_account:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-student-menu_conferencing:<front>
  $menu_links['menu-student-menu_conferencing:<front>'] = array(
    'menu_name' => 'menu-student-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Conferencing',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'sites/all/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-student-menu_conferencing:<front>',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-student-menu_curriculum-plan:<front>
  $menu_links['menu-student-menu_curriculum-plan:<front>'] = array(
    'menu_name' => 'menu-student-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Curriculum Plan',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'sites/all/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-student-menu_curriculum-plan:<front>',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-student-menu_discussion-board:<front>
  $menu_links['menu-student-menu_discussion-board:<front>'] = array(
    'menu_name' => 'menu-student-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Discussion Board',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'profiles/ots/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-student-menu_discussion-board:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-student-menu_documents:dashboard/documents
  $menu_links['menu-student-menu_documents:dashboard/documents'] = array(
    'menu_name' => 'menu-student-menu',
    'link_path' => 'dashboard/documents',
    'router_path' => 'dashboard/documents',
    'link_title' => 'Documents',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'profiles/ots/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-student-menu_documents:dashboard/documents',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-student-menu_documents:menutoken/55f687eb46356
  $menu_links['menu-student-menu_documents:menutoken/55f687eb46356'] = array(
    'menu_name' => 'menu-student-menu',
    'link_path' => 'menutoken/55f687eb46356',
    'router_path' => 'menutoken/%',
    'link_title' => 'Documents',
    'options' => array(
      'menu_token_link_path' => 'dashboard/students/[user:uid]/documents',
      'menu_token_data' => array(
        'user' => array(
          'type' => 'user',
          'plugin' => 'user_context',
          'options' => array(),
        ),
      ),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'profiles/ots/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-student-menu_documents:menutoken/55f687eb46356',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-student-menu_resources:<front>
  $menu_links['menu-student-menu_resources:<front>'] = array(
    'menu_name' => 'menu-student-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Resources',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'profiles/ots/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-student-menu_resources:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: menu-student-menu_lesson-planner:<front>
  $menu_links['menu-student-menu_lesson-planner:<front>'] = array(
    'menu_name' => 'menu-student-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Lesson Planner',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'profiles/ots/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-student-menu_lesson-planner:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-student-menu_resources:<front>
  $menu_links['menu-student-menu_resources:<front>'] = array(
    'menu_name' => 'menu-student-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Resources',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'sites/all/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-student-menu_resources:<front>',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
  );
  // Exported menu link: menu-student-menu_schedule:dashboard/schedule
  $menu_links['menu-student-menu_schedule:dashboard/schedule'] = array(
    'menu_name' => 'menu-student-menu',
    'link_path' => 'dashboard/schedule',
    'router_path' => 'dashboard/schedule',
    'link_title' => 'Schedule',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'sites/all/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-student-menu_schedule:dashboard/schedule',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Account');
  t('Conferencing');
  t('Curriculum Plan');
  t('Dictionary');
  t('Discussion Board');
  t('Documents');
  t('Lesson Planner');
  t('Resources');
  t('Flash Card App');
  t('Schedule');
  t('Text Book');

  return $menu_links;
}
