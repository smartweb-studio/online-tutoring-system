<?php
/**
 * @file
 * feature_tutor_menus.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function feature_tutor_menus_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-tutor-menu.
  $menus['menu-tutor-menu'] = array(
    'menu_name' => 'menu-tutor-menu',
    'title' => 'role tutor menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('role tutor menu');


  return $menus;
}
