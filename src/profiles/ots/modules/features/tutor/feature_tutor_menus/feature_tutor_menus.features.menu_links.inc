<?php
/**
 * @file
 * feature_tutor_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function feature_tutor_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-student-menu_documents:dashboard/documents
  $menu_links['menu-student-menu_documents:dashboard/documents'] = array(
    'menu_name' => 'menu-student-menu',
    'link_path' => 'dashboard/documents',
    'router_path' => 'dashboard/documents',
    'link_title' => 'Documents',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'profiles/ots/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-student-menu_documents:dashboard/documents',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-student-menu_documents:menutoken/55f687eb46356
  $menu_links['menu-student-menu_documents:menutoken/55f687eb46356'] = array(
    'menu_name' => 'menu-student-menu',
    'link_path' => 'menutoken/55f687eb46356',
    'router_path' => 'menutoken/%',
    'link_title' => 'Documents',
    'options' => array(
      'menu_token_link_path' => 'dashboard/students/[user:uid]/documents',
      'menu_token_data' => array(
        'user' => array(
          'type' => 'user',
          'plugin' => 'user_context',
          'options' => array(),
        ),
      ),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'profiles/ots/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-student-menu_documents:menutoken/55f687eb46356',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-tutor-menu_account:<front>
  $menu_links['menu-tutor-menu_account:<front>'] = array(
    'menu_name' => 'menu-tutor-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Account',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'sites/all/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-tutor-menu_account:<front>',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 1,
  );
  // Exported menu link: menu-tutor-menu_add-lesson:node/add/lesson
  $menu_links['menu-tutor-menu_add-lesson:node/add/lesson'] = array(
    'menu_name' => 'menu-tutor-menu',
    'link_path' => 'node/add/lesson',
    'router_path' => 'node/add/lesson',
    'link_title' => 'Add Lesson',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'profiles/ots/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-tutor-menu_add-lesson:node/add/lesson',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-tutor-menu_conferencing:<front>
  $menu_links['menu-tutor-menu_conferencing:<front>'] = array(
    'menu_name' => 'menu-tutor-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Conferencing',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'sites/all/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-tutor-menu_conferencing:<front>',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-tutor-menu_discussion-board:<front>
  $menu_links['menu-tutor-menu_discussion-board:<front>'] = array(
    'menu_name' => 'menu-tutor-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Discussion Board',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'sites/all/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-tutor-menu_discussion-board:<front>',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 4,
    'customized' => 1,
  );
  // Exported menu link: menu-tutor-menu_schedule:dashboard/schedule
  $menu_links['menu-tutor-menu_schedule:dashboard/schedule'] = array(
    'menu_name' => 'menu-tutor-menu',
    'link_path' => 'dashboard/schedule',
    'router_path' => 'dashboard/schedule',
    'link_title' => 'Schedule',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'menu_icon' => array(
        'enable' => 0,
        'path' => 'sites/all/modules/contrib/menu_icons/images/default_icon.png',
        'image_style' => 'menu_icon',
      ),
      'identifier' => 'menu-tutor-menu_schedule:dashboard/schedule',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Account');
  t('Add Lesson');
  t('Conferencing');
  t('Discussion Board');
  t('Documents');
  t('Schedule');

  return $menu_links;
}
