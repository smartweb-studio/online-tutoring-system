<?php
/**
 * @file
 * feature_tutor_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function feature_tutor_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
