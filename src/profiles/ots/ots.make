; Drupal core.
core = 7.x
projects[] = drupal

; Drush Make API version.
api = 2

; Modules
projects[addressfield][subdir]           = "contrib"
projects[addressfield][version]          = "1.1"

projects[admin_menu][subdir]             = "contrib"
projects[admin_menu][version]            = "3.0-rc5"

projects[calendar][subdir]               = "contrib"
projects[calendar][version]              = "3.5"

projects[chosen][subdir]                 = "contrib"
projects[chosen][version]                = "2.0-beta4"

projects[ctools][subdir]                 = "contrib"
projects[ctools][version]                = "1.7"

projects[date][subdir]                   = "contrib"
projects[date][version]                  = "2.8"

projects[datepicker][subdir]             = "contrib"
projects[datepicker][version]            = "1.0"

projects[date_popup_timepicker][subdir]  = "contrib"
projects[date_popup_timepicker][version] = "1.0-beta1"

projects[date_restrictions][subdir]      = "contrib"
projects[date_restrictions][version]     = "1.x-dev"

projects[editablefields][subdir]         = "contrib"
projects[editablefields][version]        = "1.0-alpha2"

projects[email_registration][subdir]     = "contrib"
projects[email_registration][version]    = "1.2"

projects[entity][subdir]                 = "contrib"
projects[entity][version]                = "1.6"

projects[entityreference][subdir]        = "contrib"
projects[entityreference][version]       = "1.1"

projects[features][subdir]               = "contrib"
projects[features][version]              = "2.6"

projects[features_roles_permissions][subdir]               = "contrib"
projects[features_roles_permissions][version]              = "1.2"

projects[features_extra][subdir]         = "contrib"
projects[features_extra][version]        = "1.0-beta1"

projects[field_permissions][subdir]       = "contrib"
projects[field_permissions][version]      = "1.0-beta2"

projects[field_validation][subdir]       = "contrib"
projects[field_validation][version]      = "2.6"

projects[file_entity][subdir]            = "contrib"
projects[file_entity][version]           = "2.0-beta1"

projects[front][subdir]                  = "contrib"
projects[front][version]                 = "2.4"

projects[imce_wysiwyg][subdir]           = "contrib"
projects[imce_wysiwyg][version]          = "1.0"

projects[imce][subdir]                   = "contrib"
projects[imce][version]                  = "1.9"

projects[jquery_colorpicker][subdir]     = "contrib"
projects[jquery_colorpicker][version]    = "1.1"

projects[jquery_update][subdir]          = "contrib"
projects[jquery_update][version]         = "2.6"

projects[jump_menu][subdir]              = "contrib"
projects[jump_menu][version]             = "1.4"

projects[libraries][subdir]              = "contrib"
projects[libraries][version]             = "2.2"

projects[link][subdir]                   = "contrib"
projects[link][version]                  = "1.3"

projects[logintoboggan][subdir]          = "contrib"
projects[logintoboggan][version]         = "1.5"

projects[menu_icons][subdir]             = "contrib"
projects[menu_icons][version]            = "3.0-beta4"

projects[menu_item_visibility][subdir]   = "contrib"
projects[menu_item_visibility][version]  = "1.0-beta1"

projects[menu_token][subdir]             = "contrib"
projects[menu_token][version]            = "1.0-beta5"

projects[module_filter][subdir]          = "contrib"
projects[module_filter][version]         = "2.0"

projects[nodeaccess_userreference][subdir]          	 = "contrib"
projects[nodeaccess_userreference][version]         	 = "3.10"

projects[node_export][subdir]          	 = "contrib"
projects[node_export][version]         	 = "3.0"

projects[panels][subdir]                 = "contrib"
projects[panels][version]                = "3.5"

projects[pathauto][subdir]               = "contrib"
projects[pathauto][version]              = "1.2"

projects[prevent_js_alerts][subdir]      = "contrib"
projects[prevent_js_alerts][version]     = "1.0"

projects[profile2][subdir]               = "contrib"
projects[profile2][version]              = "1.3"

projects[profile2_regpath][subdir]       = "contrib"
projects[profile2_regpath][version]      = "1.12"

projects[rules][subdir]       = "contrib"
projects[rules][version]      = "2.9"

projects[strongarm][subdir]              = "contrib"
projects[strongarm][version]             = "2.0"

projects[token][subdir]                  = "contrib"
projects[token][version]                 = "1.6"

projects[uuid][subdir]                   = "contrib"
projects[uuid][version]                  = "1.0-alpha6"

projects[variable][subdir]               = "contrib"
projects[variable][version]              = "2.5"

projects[views][subdir]                  = "contrib"
projects[views][version]                 = "3.11"

projects[views_bulk_operations][subdir]                  = "contrib"
projects[views_bulk_operations][version]                 = "3.3"

projects[views_linkarea][subdir]         = "contrib"
projects[views_linkarea][version]        = "1.0"

projects[viewfield][subdir]              = "contrib"
projects[viewfield][version]             = "2.0"

projects[visualization][subdir]          = "contrib"
projects[visualization][version]         = "1.0-beta2"

projects[weekdays][subdir]               = "contrib"
projects[weekdays][version]              = "1.1"

projects[workflow][subdir]                = "contrib"
projects[workflow][version]               = "2.5"

projects[wysiwyg][subdir]                = "contrib"
projects[wysiwyg][version]               = "2.2"

projects[workflow][subdir]                = "contrib"
projects[workflow][version]               = "2.5"

projects[views_php][subdir]            = "contrib"
projects[views_php][version]           = "1.0-alpha1"

; Development
projects[devel][subdir]                  = "contrib"
projects[devel][version]                 = "1.5"

projects[environment][subdir]            = "contrib"
projects[environment][version]           = "1.0"

; Libraries
; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.

libraries[dropbox_sdk][download][type]   = "git"
libraries[dropbox_sdk][download][url]    = "https://github.com/dropbox/dropbox-sdk-php.git"
libraries[dropbox_sdk][directory_name]   = "dropbox-sdk"
libraries[dropbox_sdk][type]             = "library"

libraries[timepicker][download][type]   = "git"
libraries[timepicker][download][url]    = "https://github.com/fgelinas/timepicker.git"
libraries[timepicker][directory_name]   = "timepicker"
libraries[timepicker][type]             = "library"

libraries[colorpicker][download][type]  = "get"
libraries[colorpicker][download][url]   = "http://www.eyecon.ro/colorpicker/colorpicker.zip"
libraries[colorpicker][directory_name]  = "colorpicker"
libraries[colorpicker][type]            = "library"

libraries[chosen][download][type]       = "get"
libraries[chosen][download][url]        = "https://github.com/harvesthq/chosen/releases/download/1.4.2/chosen_v1.4.2.zip"
libraries[chosen][directory_name]       = "chosen"
libraries[chosen][type]                 = "library"

libraries[highcharts][download][type]   = "get"
libraries[highcharts][download][url]    = "https://github.com/highslide-software/highcharts.com/archive/master.zip"
libraries[highcharts][directory_name]   = "highcharts"
libraries[highcharts][type]             = "library"

; Themes
projects[] = "adminimal_theme"
