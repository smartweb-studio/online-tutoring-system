<?php
/**
 * @file
 * Enables modules and site configuration for a ots site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function ots_form_install_configure_form_alter(&$form, $form_state) {

}
